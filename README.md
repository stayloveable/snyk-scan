# Bitbucket Pipelines Pipe: Snyk

This pipe uses Snyk to find, fix and monitor known vulnerabilities in your app dependencies and docker image.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: snyk/snyk-scan:0.3.6
  variables:
    SNYK_TOKEN: "<string>"
    LANGUAGE: "<string>"
    # IMAGE_NAME: "<string>" # Only required if LANGUAGE set to "docker"
    # PROTECT: "<boolean>" # Optional.
    # DONT_BREAK_BUILD: "<boolean>" # Optional.
    # CODE_INSIGHTS_RESULTS: "<boolean>" # Optional.
    # MONITOR: "<boolean>" # Optional.
    # SEVERITY_THRESHOLD: "<low|medium|high>" # Optional.
    # ORGANIZATION: "<string>" # Optional.
    # PROJECT_FOLDER: "<string>" # Optional.
    # TARGET_FILE: "<string>" # Optional.
    # EXTRA_ARGS: "<string>" # Optional.
    # DEBUG: "<boolean>" # Optional.
```

## Variables

| Variable              | Usage                                     |
| --------------        | ----------------------------------------- |
| SNYK_TOKEN (\*)       | The Snyk API Token. |
| LANGUAGE (\*)         | The package manager of the app (e.g. `npm`, `rubygems`, `composer`, `nuget` or `docker`). See [Dockerhub](https://hub.docker.com/r/snyk/snyk-cli/tags) for a  full list of possible tags. |
| IMAGE_NAME (\*)       | For `docker` language, the image to do a docker scan on. |
| PROTECT               | This will apply the patches specified in your .snyk file to the local file system. (After running [Snyk Wizard](https://snyk.io/docs/cli-wizard/)) Default: `false`. |
| DONT_BREAK_BUILD      | Continue build despite issues found. Default: `false`. |
| CODE_INSIGHTS_RESULTS | Create Code Insight report with Snyk test results. Default: `false`. |
| MONITOR               | Record a snapshot of the project to the Snyk UI and keep monitoring it after initial test. Default: `false`. |
| SEVERITY_THRESHOLD    | Reports on issues equal or higher of the provided level. Allowed Values: `low`, `med`, `high`. Default: `low`. |
| ORGANIZATION          | Organization to run the cli with. Default: none. |
| PROJECT_FOLDER        | The folder in which the project resides. Default: `.`. |
| TARGET_FILE           | The package file (e.g. `package.json`); equivalent to `--file=` in the CLI. For Docker should point to the `Dockerfile`. Default: none. |
| EXTRA_ARGS            | Extra arguments to be passed to the snyk cli. Default: none. |
| DEBUG                 | Turn on extra debug information. Default: `false`. |

_(\*) = required variable._

## Details

By integrating Snyk into Bitbucket Pipelines, you can continuously test, fix and monitor your codebase for known vulnerabilities in your dependencies and docker image.

To find out more about Snyk, head over to [our website](https://snyk.io).

## Prerequisites

Snyk API Token is necessary to use this pipe.

- If you don't yet have an account on snyk.io, head over and [register](https://app.snyk.io/signup).
- To obtain a token log in to your Snyk account and obtain an API token from the [account page](https://app.snyk.io/account).
- Add the generated token as a [secured environment variable](https://confluence.atlassian.com/x/0CVbLw#Environmentvariables-Securedvariables) in Bitbucket Pipelines.

## Examples

### Basic app dependencies scan example
Uses Snyk to scan a Node.js application and break the build if any vulnerabilities found.

```yaml
script:
  - npm install

  - npm test

  - pipe: snyk/snyk-scan:0.3.6
    variables:
      SNYK_TOKEN: $SNYK_TOKEN
      LANGUAGE: "npm"

  - npm publish
```

### Basic Docker image scan example
Uses Snyk to scan a Docker image and break the build if any vulnerabilities found.

```yaml
script:
  - docker build -t $IMAGE_NAME .

  - pipe: snyk/snyk-scan:0.3.6
    variables:
      SNYK_TOKEN: $SNYK_TOKEN
      LANGUAGE: "docker"
      IMAGE_NAME: $IMAGE_NAME
      TARGET_FILE: "Dockerfile"

  - docker push $IMAGE_NAME
```

### Advanced example
Apply Snyk patches to a Node.js app, and continue to scan the dependencies for *high severity* vulnerabilities only, but not failing the build if found any. Also opt-in to monitor the dependencies states on Snyk.io to get alerts if new vulns found. Then go on to build the Docker image and scan it for *high severity* vulnerabilities.

```yaml
script:
  - npm install

  - npm test

  - pipe: snyk/snyk-scan:0.3.6
    variables:
      SNYK_TOKEN: $SNYK_TOKEN
      LANGUAGE: "npm"
      PROTECT: "true"
      SEVERITY_THRESHOLD: "high"
      DONT_BREAK_BUILD: "true"
      MONITOR: "true"

  - docker build -t $IMAGE_NAME .

  - pipe: snyk/snyk-scan:0.3.6
    variables:
      SNYK_TOKEN: $SNYK_TOKEN
      LANGUAGE: "docker"
      IMAGE_NAME: $IMAGE_NAME
      TARGET_FILE: "Dockerfile"
      SEVERITY_THRESHOLD: "high"
      DONT_BREAK_BUILD: "true"
      MONITOR: "true"

  - docker push $IMAGE_NAME

```

## Support
If you’d like help with this pipe, or you have an issue or feature request, please contact us at: [support@snyk.io](support@snyk.io).

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce

## License
Copyright (c) 2019 Snyk Ltd. and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.
