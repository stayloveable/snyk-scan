import * as _ from "lodash";
import {
  ReportDataType,
  ReportResult,
  ReportType,
  ReportHeader,
  AnnotationType,
  Severity,
  SeverityOrder
} from "./common/types";

const MAX_ISSUES_PER_REPORT = 1000;
export const SNYK_VULN_PAGE = "https://app.snyk.io/vuln";
export const LOGO_URL =
  "https://res.cloudinary.com/snyk/image/upload/f_auto,q_auto,w_48/v1468845259/logo/snyk-avatar.svg";

export const generateReport = (
  bitbucketRepoOwner: string,
  bitbucketRepoSlug: string,
  bitbucketBuildNumber: string,
  snykOutput: any
): object => {
  const issues = getIssuesList(snykOutput);
  const totalIssues = issues?.length ?? 0;
  const groupedIssues = _.groupBy(issues, "severity");
  const reportResult = totalIssues ? ReportResult.FAILED : ReportResult.PASSED;
  const reportType = "security";
  const title = `${reportType}/snyk (${getOrg(snykOutput)})`;

  return {
    type: "report",
    report_type: ReportType.SECURITY, // eslint-disable-line @typescript-eslint/camelcase
    reporter: ReportHeader.SNYK,
    result: reportResult,
    logo_url: LOGO_URL, // eslint-disable-line @typescript-eslint/camelcase
    title: title,
    link: createLinkUrl(
      bitbucketRepoOwner,
      bitbucketRepoSlug,
      bitbucketBuildNumber
    ),
    details: detailsContent(totalIssues),
    data: dataContent(totalIssues, groupedIssues)
  };
};

export function parseSeverityOrder(severity: string): SeverityOrder {
  const lcSev = severity.toLowerCase();
  if (lcSev === "high") {
    return SeverityOrder.high;
  } else if (lcSev === "medium") {
    return SeverityOrder.medium;
  } else if (lcSev === "low") {
    return SeverityOrder.low;
  } else {
    throw new Error(`invalid severity: ${severity}`);
  }
}

export const generateAnnotations = (snykOutput: any): object => {
  const issues = getIssuesList(snykOutput);
  const targetFile = snykOutput.displayTargetFile;

  const sortedIssues = issues.sort((prev, next) => {
    const prevSev = parseSeverityOrder(prev.severity);
    const nextSev = parseSeverityOrder(next.severity);
    return prevSev - nextSev;
  });

  return sortedIssues.slice(0, MAX_ISSUES_PER_REPORT).map((issue, index) => ({
    type: "report_annotation",
    annotation_type: AnnotationType.VULNERABILITY, // eslint-disable-line @typescript-eslint/camelcase
    external_id: `${index}:${issue.id}`, // eslint-disable-line @typescript-eslint/camelcase
    summary: `${issue.name}@${issue.version}: ${issue.title}`,
    severity: Severity[issue.severity],
    path: targetFile,
    link: `${SNYK_VULN_PAGE}/${issue.id}`,
    details: `Snyk vulnerability ID: ${issue.id}`
  }));
};

const getOrg = snykOutput => {
  if (!Array.isArray(snykOutput)) {
    snykOutput = [snykOutput];
  }

  return snykOutput[0]?.org ?? "";
};

const getIssuesList = (snykOutput): any => {
  if (!Array.isArray(snykOutput)) {
    snykOutput = [snykOutput];
  }

  let issues = [];
  snykOutput.forEach(output => {
    issues = issues.concat(_.uniqBy(output.vulnerabilities, "id"));
  });

  return issues;
};

const detailsContent = (totalIssues: number): string =>
  `This pull request introduces ${totalIssues} ${pluralizeIssueWord(
    totalIssues
  )}`;
const pluralizeIssueWord = (totalIssues: number): string =>
  totalIssues === 1 ? "issue" : "issues";

const createLinkUrl = (
  bitbucketRepoOwner,
  bitbucketRepoSlug,
  bitbucketBuildNumber
): URL =>
  new URL(
    `https://bitbucket.org/${bitbucketRepoOwner}/${bitbucketRepoSlug}/addon/pipelines/home#!/results/${bitbucketBuildNumber}`
  );
const dataContent = (totalIssues, groupedIssues) => [
  {
    title: ReportHeader.TOTAL,
    type: ReportDataType.NUMBER,
    value: totalIssues
  },
  {
    title: ReportHeader.HIGH_SEVERITY,
    type: ReportDataType.NUMBER,
    value: groupedIssues.high?.length ?? 0
  },
  {
    title: ReportHeader.MEDIUM_SEVERITY,
    type: ReportDataType.NUMBER,
    value: groupedIssues.medium?.length ?? 0
  },
  {
    title: ReportHeader.LOW_SEVERITY,
    type: ReportDataType.NUMBER,
    value: groupedIssues.low?.length ?? 0
  }
];
