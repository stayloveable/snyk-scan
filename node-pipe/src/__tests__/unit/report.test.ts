import * as fs from "fs";
import {
  ReportDataType,
  ReportType,
  ReportResult,
  ReportHeader
} from "../../common/types";

import {
  generateReport,
  LOGO_URL,
  SNYK_VULN_PAGE,
  generateAnnotations
} from "../../report";

const fixturesBasePath = "./src/__tests__/fixtures";
const bitbucketRepoOwner = "snyk";
const bitbucketRepoSlug = "snyk-scan";
const bitbucketBuildNumber = "1";
const buildUrl = new URL(
  `https://bitbucket.org/${bitbucketRepoOwner}/${bitbucketRepoSlug}/addon/pipelines/home#!/results/${bitbucketBuildNumber}`
);

describe("convert snyk output to bitbucket api input", () => {
  describe("for report", () => {
    describe("when there is NO vulnerabilities", () => {
      const snykOutputFilePath = `${fixturesBasePath}/snyk_output_without_vuln.json`;
      const snykOutput: any = JSON.parse(
        fs.readFileSync(snykOutputFilePath, "utf-8")
      );
      it("should show 0 vulnerabilities", () => {
        const bitbucketInput: object = {
          type: "report",
          report_type: ReportType.SECURITY, // eslint-disable-line @typescript-eslint/camelcase
          reporter: ReportHeader.SNYK,
          result: ReportResult.PASSED,
          logo_url: LOGO_URL, // eslint-disable-line @typescript-eslint/camelcase
          title: "security/snyk (Snyk)",
          link: buildUrl,
          details: "This pull request introduces 0 issues",
          data: [
            {
              title: ReportHeader.TOTAL,
              type: ReportDataType.NUMBER,
              value: 0
            },
            {
              title: ReportHeader.HIGH_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 0
            },
            {
              title: ReportHeader.MEDIUM_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 0
            },
            {
              title: ReportHeader.LOW_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 0
            }
          ]
        };
        const result: object = generateReport(
          bitbucketRepoOwner,
          bitbucketRepoSlug,
          bitbucketBuildNumber,
          snykOutput
        );

        expect(result).toEqual(bitbucketInput);
      });
    });

    describe("when there in 1 vulnerabilities", () => {
      const snykOutputFilePath = `${fixturesBasePath}/snyk_output_with_vuln.json`;
      const snykOutput: any = JSON.parse(
        fs.readFileSync(snykOutputFilePath, "utf-8")
      );

      it("should show 1 vulnerability in the report", () => {
        const bitbucketInput: object = {
          type: "report",
          report_type: ReportType.SECURITY, // eslint-disable-line @typescript-eslint/camelcase
          reporter: ReportHeader.SNYK,
          result: ReportResult.FAILED,
          logo_url: LOGO_URL, // eslint-disable-line @typescript-eslint/camelcase
          title: "security/snyk (Snyk)",
          link: buildUrl,
          details: "This pull request introduces 1 issue",
          data: [
            {
              title: ReportHeader.TOTAL,
              type: ReportDataType.NUMBER,
              value: 1
            },
            {
              title: ReportHeader.HIGH_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 0
            },
            {
              title: ReportHeader.MEDIUM_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 0
            },
            {
              title: ReportHeader.LOW_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 1
            }
          ]
        };

        const result: object = generateReport(
          bitbucketRepoOwner,
          bitbucketRepoSlug,
          bitbucketBuildNumber,
          snykOutput
        );

        expect(result).toEqual(bitbucketInput);
      });
    });

    describe("when there is 2 vulnerabilities, but with same Snyk ID", () => {
      const snykOutputFilePath = `${fixturesBasePath}/snyk_output_with_2_vuln_but_equal_snyk_id.json`;
      const snykOutput: any = JSON.parse(
        fs.readFileSync(snykOutputFilePath, "utf-8")
      );

      it("should show 1 vulnerability in the report", () => {
        const bitbucketInput: object = {
          type: "report",
          report_type: ReportType.SECURITY, // eslint-disable-line @typescript-eslint/camelcase
          reporter: ReportHeader.SNYK,
          result: ReportResult.FAILED,
          logo_url: LOGO_URL, // eslint-disable-line @typescript-eslint/camelcase
          title: "security/snyk (Snyk)",
          link: buildUrl,
          details: "This pull request introduces 1 issue",
          data: [
            {
              title: ReportHeader.TOTAL,
              type: ReportDataType.NUMBER,
              value: 1
            },
            {
              title: ReportHeader.HIGH_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 0
            },
            {
              title: ReportHeader.MEDIUM_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 0
            },
            {
              title: ReportHeader.LOW_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 1
            }
          ]
        };

        const result: object = generateReport(
          bitbucketRepoOwner,
          bitbucketRepoSlug,
          bitbucketBuildNumber,
          snykOutput
        );

        expect(result).toEqual(bitbucketInput);
      });
    });

    describe("when there are multi manifest with 2 different vulnerabilities", () => {
      const snykOutputFilePath = `${fixturesBasePath}/snyk_output_multi_projects.json`;
      const snykOutput: any = JSON.parse(
        fs.readFileSync(snykOutputFilePath, "utf-8")
      );

      it("should show 2 vulnerability in the report", () => {
        const bitbucketInput: object = {
          type: "report",
          report_type: ReportType.SECURITY, // eslint-disable-line @typescript-eslint/camelcase
          reporter: ReportHeader.SNYK,
          result: ReportResult.FAILED,
          logo_url: LOGO_URL, // eslint-disable-line @typescript-eslint/camelcase
          title: "security/snyk (demo-applications)",
          link: buildUrl,
          details: "This pull request introduces 2 issues",
          data: [
            {
              title: ReportHeader.TOTAL,
              type: ReportDataType.NUMBER,
              value: 2
            },
            {
              title: ReportHeader.HIGH_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 2
            },
            {
              title: ReportHeader.MEDIUM_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 0
            },
            {
              title: ReportHeader.LOW_SEVERITY,
              type: ReportDataType.NUMBER,
              value: 0
            }
          ]
        };

        const result: object = generateReport(
          bitbucketRepoOwner,
          bitbucketRepoSlug,
          bitbucketBuildNumber,
          snykOutput
        );

        expect(result).toEqual(bitbucketInput);
      });
    });
  });

  describe("for annotations", () => {
    describe("when there is NO vulnerabilities", () => {
      const snykOutputFilePath = `${fixturesBasePath}/snyk_output_without_vuln.json`;
      const snykOutput: object = JSON.parse(
        fs.readFileSync(snykOutputFilePath, "utf-8")
      );

      it("should show 0 vulnerability in the annotation list", () => {
        const bitbucketInput: Array<object> = [];

        const result: object = generateAnnotations(snykOutput);

        expect(result).toEqual(bitbucketInput);
      });
    });

    describe("when there is 1 vulnerabilities", () => {
      const snykOutputFilePath = `${fixturesBasePath}/snyk_output_with_vuln.json`;
      const snykOutput: object = JSON.parse(
        fs.readFileSync(snykOutputFilePath, "utf-8")
      );

      it("should show 1 vulnerability in the annotation list", () => {
        const issue = snykOutput["vulnerabilities"][0];
        const bitbucketInput: Array<object> = [
          {
            type: "report_annotation",
            annotation_type: "VULNERABILITY", // eslint-disable-line @typescript-eslint/camelcase
            external_id: `0:${issue.id}`, // eslint-disable-line @typescript-eslint/camelcase
            summary: `${issue.name}@${issue.version}: ${issue.title}`,
            severity: "LOW",
            path: snykOutput["displayTargetFile"],
            link: `${SNYK_VULN_PAGE}/${issue.id}`,
            details: `Snyk vulnerability ID: ${issue.id}`
          }
        ];

        const result: object = generateAnnotations(snykOutput);

        expect(result).toEqual(bitbucketInput);
      });
    });

    describe("when there is 2 vulnerabilities, but with same Snyk ID", () => {
      const snykOutputFilePath = `${fixturesBasePath}/snyk_output_with_2_vuln_but_equal_snyk_id.json`;
      const snykOutput: object = JSON.parse(
        fs.readFileSync(snykOutputFilePath, "utf-8")
      );

      it("should show 1 vulnerability in the annotation list", () => {
        const issue = snykOutput["vulnerabilities"][0];
        const bitbucketInput: Array<object> = [
          {
            type: "report_annotation",
            annotation_type: "VULNERABILITY", // eslint-disable-line @typescript-eslint/camelcase
            external_id: `0:${issue.id}`, // eslint-disable-line @typescript-eslint/camelcase
            summary: `${issue.name}@${issue.version}: ${issue.title}`,
            severity: "LOW",
            path: snykOutput["displayTargetFile"],
            link: `${SNYK_VULN_PAGE}/${issue.id}`,
            details: `Snyk vulnerability ID: ${issue.id}`
          }
        ];

        const result: object = generateAnnotations(snykOutput);

        expect(result).toEqual(bitbucketInput);
      });
    });

    describe("when there are multi manifest with 2 different vulnerabilities", () => {
      const snykOutputFilePath = `${fixturesBasePath}/snyk_output_multi_projects.json`;
      const snykOutput: any = JSON.parse(
        fs.readFileSync(snykOutputFilePath, "utf-8")
      );

      it("should show 2 vulnerability in the report", () => {
        const issue1 = snykOutput[0]["vulnerabilities"][0];
        const issue2 = snykOutput[1]["vulnerabilities"][0];
        const bitbucketInput: Array<object> = [
          {
            type: "report_annotation",
            annotation_type: "VULNERABILITY", // eslint-disable-line @typescript-eslint/camelcase
            external_id: `0:${issue1.id}`, // eslint-disable-line @typescript-eslint/camelcase
            summary: `${issue1.name}@${issue1.version}: ${issue1.title}`,
            severity: "HIGH",
            path: snykOutput["displayTargetFile"],
            link: `${SNYK_VULN_PAGE}/${issue1.id}`,
            details: `Snyk vulnerability ID: ${issue1.id}`
          },
          {
            type: "report_annotation",
            annotation_type: "VULNERABILITY", // eslint-disable-line @typescript-eslint/camelcase
            external_id: `1:${issue2.id}`, // eslint-disable-line @typescript-eslint/camelcase
            summary: `${issue2.name}@${issue2.version}: ${issue2.title}`,
            severity: "HIGH",
            path: snykOutput["displayTargetFile"],
            link: `${SNYK_VULN_PAGE}/${issue2.id}`,
            details: `Snyk vulnerability ID: ${issue2.id}`
          }
        ];

        const result: object = generateAnnotations(snykOutput);

        expect(result).toEqual(bitbucketInput);
      });
    });
  });
});
