import * as fs from "fs";
import { main } from "../../main";
import { generateReport, generateAnnotations } from "../../report";
import { generateMd5Key } from "../../common/helpers";
import { BitbucketApiResponse } from "../../bitbucket-client";

describe("Create Code Insight Report", () => {
  const fixturesBasePath = "./src/__tests__/fixtures";
  const bitbucketRepoOwner = "snykTeam";
  const bitbucketRepoSlug = "snyk-scan";
  const commitHash = "valid-commit-hash";
  const bitbucketBuildNumber = "valid-id";
  const reportType = "security";
  const externalId = generateMd5Key(`${reportType}:${commitHash}`);
  const mockPutReportResponse = {
    statusCode: 200,
    jsonObj: { msg: "nice report" }
  } as BitbucketApiResponse;

  test("When there are No vulnerabilities", async () => {
    const putReportMock = jest.fn().mockResolvedValue(mockPutReportResponse);
    const postAllAnnotationsMock = jest.fn(() => 0);
    const snykOutputFilePath = `${fixturesBasePath}/snyk_output_without_vuln.json`;
    const codeInsightInput = generateReport(
      bitbucketRepoOwner,
      bitbucketRepoSlug,
      bitbucketBuildNumber,
      JSON.parse(fs.readFileSync(snykOutputFilePath, "utf-8"))
    );
    const expectedResult = 0;

    const result = await main(
      bitbucketRepoOwner,
      bitbucketRepoSlug,
      commitHash,
      bitbucketBuildNumber,
      snykOutputFilePath,
      generateReport,
      putReportMock,
      generateAnnotations,
      postAllAnnotationsMock
    );

    expect(putReportMock).toHaveBeenCalledWith(
      bitbucketRepoOwner,
      bitbucketRepoSlug,
      commitHash,
      externalId,
      codeInsightInput
    );

    expect(postAllAnnotationsMock).toHaveBeenCalledTimes(0);
    expect(result).toEqual(expectedResult);
  });

  test("When there are vulnerabilities", async () => {
    const putReportMock = jest.fn().mockResolvedValue(mockPutReportResponse);
    const postAllAnnotationsMock = jest.fn(() => 0);
    const snykOutputFilePath = `${fixturesBasePath}/snyk_output_with_vuln.json`;
    const snykTestOutputJson = JSON.parse(
      fs.readFileSync(snykOutputFilePath, "utf-8")
    );
    const codeInsightInput = generateReport(
      bitbucketRepoOwner,
      bitbucketRepoSlug,
      bitbucketBuildNumber,
      snykTestOutputJson
    );
    const codeInsightsAnnotations = generateAnnotations(snykTestOutputJson);
    const expectedResult = 0;

    const result = await main(
      bitbucketRepoOwner,
      bitbucketRepoSlug,
      commitHash,
      bitbucketBuildNumber,
      snykOutputFilePath,
      generateReport,
      putReportMock,
      generateAnnotations,
      postAllAnnotationsMock
    );

    expect(putReportMock).toHaveBeenCalledWith(
      bitbucketRepoOwner,
      bitbucketRepoSlug,
      commitHash,
      externalId,
      codeInsightInput
    );

    expect(postAllAnnotationsMock).toHaveBeenCalledWith(
      bitbucketRepoOwner,
      bitbucketRepoSlug,
      commitHash,
      externalId,
      codeInsightsAnnotations
    );

    expect(result).toEqual(expectedResult);
  });

  test("When snyk test cannot scan because a valid error", async () => {
    const putReportMock = jest.fn().mockResolvedValue(mockPutReportResponse);
    const postAllAnnotationsMock = jest.fn(() => 0);
    const snykOutputFilePath = `${fixturesBasePath}/snyk_output_with_error.json`;
    const expectedErrorMessage = "Error: Error to snyk test scan application!";

    const callMain = async () => {
      await main(
        bitbucketRepoOwner,
        bitbucketRepoSlug,
        commitHash,
        bitbucketBuildNumber,
        snykOutputFilePath,
        generateReport,
        putReportMock,
        generateAnnotations,
        postAllAnnotationsMock
      );
    };

    await expect(callMain()).rejects.toThrowError(expectedErrorMessage);
    expect(putReportMock).not.toHaveBeenCalled();
  });

  test("When there is a unexpected snyk output", async () => {
    const putReportMock = jest.fn().mockResolvedValue(mockPutReportResponse);
    const postAllAnnotationsMock = jest.fn(() => 0);
    const snykOutputFilePath = `${fixturesBasePath}/invalid_snyk_output.txt`;
    const expectedErrorMessage =
      "SyntaxError: Unexpected token C in JSON at position 0";

    const callMain = async () => {
      await main(
        bitbucketRepoOwner,
        bitbucketRepoSlug,
        commitHash,
        bitbucketBuildNumber,
        snykOutputFilePath,
        generateReport,
        putReportMock,
        generateAnnotations,
        postAllAnnotationsMock
      );
    };

    await expect(callMain()).rejects.toThrowError(expectedErrorMessage);
    expect(putReportMock).not.toHaveBeenCalled();
  });
});
