import * as fs from "fs";
import { generateMd5Key, isDebug } from "./common/helpers";
import { generateReport, generateAnnotations } from "./report";
import {
  postAllAnnotations,
  putReport,
  BitbucketApiResponse
} from "./bitbucket-client";

export async function main(
  bitbucketRepoOwner: string,
  bitbucketRepoSlug: string,
  commitHash: string,
  bitbucketBuildNumber: string,
  snykOutputFilePath: string,
  generateReport: Function,
  sendReport: (
    bitbucketTeam: string,
    repoSlug: string,
    commitHash: string,
    externalId: string,
    reportPayload: any
  ) => Promise<BitbucketApiResponse>,
  generateAnnotations: (any) => object,
  sendAnnotations: Function
) {
  let rawSnykOutput = "";
  try {
    console.log("Start generating Code Insight report...");
    rawSnykOutput = fs.readFileSync(snykOutputFilePath, "utf-8");
    const snykOutput = JSON.parse(rawSnykOutput);
    if (snykOutput.hasOwnProperty("error")) {
      throw new Error("Error to snyk test scan application!");
    }

    const codeInsightReport = generateReport(
      bitbucketRepoOwner,
      bitbucketRepoSlug,
      bitbucketBuildNumber,
      snykOutput
    );
    const reportType = "security";
    const externalId = generateMd5Key(`${reportType}:${commitHash}`);

    const sendReportResult = await sendReport(
      bitbucketRepoOwner,
      bitbucketRepoSlug,
      commitHash,
      externalId,
      codeInsightReport
    );

    if (
      sendReportResult.statusCode === 200 ||
      sendReportResult.statusCode === 201
    ) {
      console.log("Code Insight report successfully generated!");
    } else {
      throw new Error(
        `Bitbucket API returned unsuccessful response code: ${sendReportResult.statusCode}`
      );
    }

    const codeInsightsAnnotations = generateAnnotations(snykOutput);
    const numAnnotations = (codeInsightsAnnotations as Array<any>).length;

    if (numAnnotations > 0) {
      await sendAnnotations(
        bitbucketRepoOwner,
        bitbucketRepoSlug,
        commitHash,
        externalId,
        codeInsightsAnnotations
      );
      console.log("Added annotations to CodeInsights report");
    }
  } catch (error) {
    console.log(rawSnykOutput);
    throw new Error(error);
  }

  return 0;
}

if (require.main === module) {
  const args = process.argv.slice(2);
  const snykOutputFilePath = args[0];
  const bitbucketRepoOwner = process.env.BITBUCKET_REPO_OWNER || "";
  const bitbucketRepoSlug = process.env.BITBUCKET_REPO_SLUG || "";
  const commitHash = process.env.BITBUCKET_COMMIT || "";
  const bitbucketBuildNumber = process.env.BITBUCKET_BUILD_NUMBER || "";

  if (isDebug()) {
    console.log("Report arguments:");
    console.log("snykOutputFilePath: ", snykOutputFilePath);
    console.log("bitbucketRepoOwner: ", bitbucketRepoOwner);
    console.log("bitbucketRepoSlug : ", bitbucketRepoSlug);
    console.log("commitHash: ", commitHash);
    console.log("bitbucketBuildNumber: ", bitbucketBuildNumber);
  }

  main(
    bitbucketRepoOwner,
    bitbucketRepoSlug,
    commitHash,
    bitbucketBuildNumber,
    snykOutputFilePath,
    generateReport,
    putReport,
    generateAnnotations,
    postAllAnnotations
  );
}
