#!/usr/bin/env bash
#
# Snyk scan pipe
#
# Required globals:
#   SNYK_TOKEN
#   LANGUAGE
#   IMAGE_NAME (only if LANGUAGE=="docker")
#
# Optional globals:
#   DONT_BREAK_BUILD (default: "false")
#   CODE_INSIGHTS_RESULTS (default "false")
#   MONITOR (default: "false")
#   SEVERITY_THRESHOLD (default: "low")
#   ORGANIZATION (default: none)
#   PROJECT_FOLDER (default: ".")
#   TARGET_FILE (default: none)
#   EXTRA_ARGS (default: "")
#   DEBUG (default: "false")
#

source "$(dirname "$0")/common.sh"

export DOCKER_HOST=$BITBUCKET_DOCKER_HOST_INTERNAL

validate() {
  # requrired system parameters
  BITBUCKET_CLONE_DIR=${BITBUCKET_CLONE_DIR:?'BITBUCKET_CLONE_DIR variable missing.'}
  BITBUCKET_REPO_SLUG=${BITBUCKET_REPO_SLUG:?'BITBUCKET_REPO_SLUG variable missing.'}

  # optional parmaeter required for LANGUAGE conditional
  SNYK_TEST_JSON_INPUT=${SNYK_TEST_JSON_INPUT:=""}

  # conditionally required parameters
  if [[ ! -n ${SNYK_TEST_JSON_INPUT} ]]; then
    LANGUAGE=${LANGUAGE:?'LANGUAGE variable missing.'}
    SNYK_TOKEN=${SNYK_TOKEN:?'SNYK_TOKEN variable missing.'}
  fi

  # potentialy required parameters
  if [[ "$LANGUAGE" == "docker" ]]; then
    IMAGE_NAME=${IMAGE_NAME:?'IMAGE_NAME variable missing.'}
  fi

  # optional parameters
  PROTECT=${PROTECT:="false"}
  DONT_BREAK_BUILD=${DONT_BREAK_BUILD:="false"}
  CODE_INSIGHTS_RESULTS=${CODE_INSIGHTS_RESULTS:="false"}
  MONITOR=${MONITOR:="false"}
  SEVERITY_THRESHOLD=${SEVERITY_THRESHOLD:="low"}
  ORGANIZATION=${ORGANIZATION:=""}
  PROJECT_FOLDER=${PROJECT_FOLDER:="."}
  TARGET_FILE=${TARGET_FILE:=""}
  EXTRA_ARGS=${EXTRA_ARGS:=""}
  DEBUG=${DEBUG:="false"}
}

run_snyk() {
  SNYK_COMMAND="$1"
  SNYK_PARAMS="${@:2}"

  PARAM_ORGANIZATION=${ORGANIZATION:+--org=$ORGANIZATION}
  PARAM_TARGET_FILE=${TARGET_FILE:+--file=$TARGET_FILE}
  PARAM_SEVERITY_THRESHOLD=${SEVERITY_THRESHOLD:+--severity-threshold=$SEVERITY_THRESHOLD}
  PARAM_DOCKER=${IMAGE_NAME:+--docker $IMAGE_NAME}

  if [[ "${DEBUG}" == "true" ]]; then
    PARAM_DEBUG="-d"
  else
    PARAM_DEBUG=""
  fi

  SNYK_PARAMS="$SNYK_PARAMS $PARAM_DEBUG $PARAM_ORGANIZATION $PARAM_DOCKER $PARAM_TARGET_FILE $PARAM_SEVERITY_THRESHOLD $EXTRA_ARGS"

  run docker container run \
    -e "SNYK_TOKEN=$SNYK_TOKEN" -w "/project" --entrypoint "snyk" \
    -v "$BITBUCKET_CLONE_DIR/$PROJECT_FOLDER:/project" \
    -e "DOCKER_HOST=$DOCKER_HOST" \
    snyk/snyk-cli:$LANGUAGE $SNYK_COMMAND $SNYK_PARAMS
}

run_protect() {
  if [[ "${PROTECT}" == "true" ]]; then
    if [ ! -f "$BITBUCKET_CLONE_DIR/$PROJECT_FOLDER/.snyk" ]; then
      info "PROTECT=true but .snyk file missing, skipping"
      return
    fi

    run_snyk protect
    echo_output

    if [[ "${status}" -eq 0 ]]; then
      success "snyk protect succeeded."
    else
      fail "snyk protect failed!"
    fi
  fi
}

run_test() {
  OUTPUT_FORMAT=""
  if [[ "${CODE_INSIGHTS_RESULTS}" == "true" ]]; then
    OUTPUT_FORMAT="--json"
  fi

  run_snyk test $OUTPUT_FORMAT

  if [[ "${CODE_INSIGHTS_RESULTS}" == "false" || "${DEBUG}" == "true" ]]; then
    echo_output
  fi

  if [[ "${CODE_INSIGHTS_RESULTS}" == "true" ]]; then
    run_code_insight_report
  fi

  if [[ "${status}" -eq 0 ]]; then
    success "snyk test succeeded."
    return
  fi

  if [[ "${output}" == *"Unauthorized"* ]]; then
    fail "Running snyk test failed. Please validate your api token"
  elif [[ "${CODE_INSIGHTS_RESULTS}" != "true" && "${output}" != *"dependencies for known"* ]]; then
    fail "Running snyk test failed."
  elif [[ "${CODE_INSIGHTS_RESULTS}" == "true" && "${output}" == *"Report Error"* ]]; then
    fail "Running snyk test failed."
  elif [[ "${DONT_BREAK_BUILD}" != "true" ]]; then
    fail "snyk test found vulnerabilities!"
  else
    info "DONT_BREAK_BUILD=$DONT_BREAK_BUILD enabled, so continuing despite found issues."
  fi
}

run_code_insight_report() {
  if [[ "${CODE_INSIGHTS_RESULTS}" == "true" ]]; then
    report
  fi
}

run_monitor() {
  if [[ "${MONITOR}" == "true" ]]; then
    run_snyk monitor --project-name=$BITBUCKET_REPO_SLUG
    echo_output

    if [[ "${status}" -eq 0 ]]; then
      success "snyk monitor succeeded."
    else
      fail "snyk monitor failed!"
    fi
  fi
}

run_only_code_insight_report() {
    echo "not running a snyk test, just creating the report"
    echo "SNYK_TEST_JSON_INPUT: ${SNYK_TEST_JSON_INPUT}"

    # need to initialize these
    status=0
    stdout_log=$(pwd)/${SNYK_TEST_JSON_INPUT}
    stderr_log="/tmp/stderr.log"

    report
}

run_pipe() {
  if [[ ! -n ${SNYK_TEST_JSON_INPUT} ]]; then
    run_protect
    run_test
    run_monitor

    echo "not running a snyk test, just creating the report"
    echo "SNYK_TEST_JSON_INPUT: ${SNYK_TEST_JSON_INPUT}"
  else
    run_only_code_insight_report
  fi
}

info "Starting pipe execution..."
enable_debug
validate
run_pipe