#!/usr/bin/env bats

setup() {
  PIPE_IMAGE=${DOCKER_IMAGE:="test/snyk-pipe"}
  run docker build -t ${PIPE_IMAGE} .
}

teardown() {
    echo "Teardown happens after each test."
}

@test "Fails on app with vulns" {
  run_pipe_container test/fixtures/npm \
    --env=LANGUAGE="npm"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -ne 0 ]
  echo $output | grep -i "known issues" | grep -i "vulnerable path"
  echo $output | grep -i -v "report error:"
  echo $output | grep -i -v "report succeed generated"
}

@test "Fails on app with vulns and generate Code Insight report" {
  run_pipe_container test/fixtures/npm \
    --env=LANGUAGE="npm" \
    --env=DEBUG="true" \
    --env=CODE_INSIGHTS_RESULTS="true"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -ne 0 ]
  echo $output | grep -i "uniqueCount"
  echo $output | grep -i "vulnerable dependency path"
  echo $output | grep -i "report succeed generated"
  echo $output | grep -i -v "report error:"
}

@test "Passes on app with vulns but with threshold" {
  run_pipe_container test/fixtures/npm \
    --env=LANGUAGE="npm" \
    --env=SEVERITY_THRESHOLD="high"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -eq 0 ]
  echo $output | grep -i "known issues" | grep -i "no vulnerable path"
  echo $output | grep -i -v "report error:"
  echo $output | grep -i -v "report succeed generated"
}

@test "Passes on app with vulns but with threshold and generate Code Insight report" {
  run_pipe_container test/fixtures/npm \
    --env=LANGUAGE="npm" \
    --env=SEVERITY_THRESHOLD="high" \
    --env=DEBUG="true" \
    --env=CODE_INSIGHTS_RESULTS="true"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -eq 0 ]
  echo $output | grep -i "uniqueCount"
  echo $output | grep -i "no high severity vulnerabilities"
  echo $output | grep -i "report succeed generated"
  echo $output | grep -i -v "report error:"
}

@test "Passes on app with vulns but with dont_break_build" {
  run_pipe_container test/fixtures/npm \
    --env=LANGUAGE="npm" \
    --env=DONT_BREAK_BUILD="true"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -eq 0 ]
  echo $output | grep -i "known issues" | grep -i "vulnerable path"
  echo $output | grep -i -v "report error:"
  echo $output | grep -i -v "report succeed generated"
}

@test "Passes on app with vulns but with dont_break_build and generate Code Insight report" {
  run_pipe_container test/fixtures/npm \
    --env=LANGUAGE="npm" \
    --env=DONT_BREAK_BUILD="true" \
    --env=DEBUG="true" \
    --env=CODE_INSIGHTS_RESULTS="true"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -eq 0 ]
  echo $output | grep -i "uniqueCount"
  echo $output | grep -i "vulnerable dependency path"
  echo $output | grep -i "report succeed generated"
  echo $output | grep -i -v "report error:"
}

@test "Protect applies patches" {
  run_pipe_container test/fixtures/npm \
    --env=LANGUAGE="npm" \
    --env=DONT_BREAK_BUILD="true" \
    --env=PROTECT="true"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -eq 0 ]
  echo $output | grep -i "applied Snyk patches"
  echo $output | grep -i -v "report error:"
  echo $output | grep -i -v "report succeed generated"
}

@test "Protect skips succesfully if no policy file" {
  run_pipe_container test/fixtures/composer \
    --env=LANGUAGE="composer" \
    --env=DONT_BREAK_BUILD="true" \
    --env=PROTECT="true"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -eq 0 ]
  echo $output | grep -i -v "report error:"
  echo $output | grep -i -v "report succeed generated"
}

@test "Monitor creates a snapshot" {
  run_pipe_container test/fixtures/npm \
    --env=LANGUAGE="npm" \
    --env=DONT_BREAK_BUILD="true" \
    --env=MONITOR="true"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -eq 0 ]
  echo $output | grep -i "Explore this snapshot"
  echo $output | grep -i -v "report error:"
  echo $output | grep -i -v "report succeed generated"
}

@test "Tests and fails on Node.js app" {
  run_pipe_container test/fixtures/npm \
    --env=LANGUAGE="npm"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -ne 0 ]
  echo $output | grep -i "package manager" | grep -i "npm"
  echo $output | grep -i -v "report error:"
  echo $output | grep -i -v "report succeed generated"
}

@test "Tests and fails on Node.js app and generate Code Insight report" {
  run_pipe_container test/fixtures/npm \
    --env=LANGUAGE="npm" \
    --env=DEBUG="true" \
    --env=CODE_INSIGHTS_RESULTS="true"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -ne 0 ]
  echo $output | grep -i "packageManager" | grep -i "npm"
  echo $output | grep -i "report succeed generated"
  echo $output | grep -i -v "report error:"
}

@test "Tests and fails on PHP app" {
  run_pipe_container test/fixtures/composer \
    --env=LANGUAGE="composer"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -ne 0 ]
  echo $output | grep -i "package manager" | grep -i "composer"
  echo $output | grep -i -v "report error:"
  echo $output | grep -i -v "report succeed generated"
}

@test "Tests and fails on PHP app and generate Code Insight report" {
  run_pipe_container test/fixtures/composer \
    --env=LANGUAGE="composer" \
    --env=DEBUG="true" \
    --env=CODE_INSIGHTS_RESULTS="true"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -ne 0 ]
  echo $output | grep -i "packageManager" | grep -i "composer"
  echo $output | grep -i "report succeed generated"
  echo $output | grep -i -v "report error:"
}

@test "Tests and fails on Ruby app" {
  run_pipe_container test/fixtures/rubygems \
    --env=LANGUAGE="rubygems"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -ne 0 ]
  echo $output | grep -i "package manager" | grep -i "rubygems"
  echo $output | grep -i -v "report error:"
  echo $output | grep -i -v "report succeed generated"
}

@test "Tests and fails on Ruby app and generate Code Insight report" {
  run_pipe_container test/fixtures/rubygems \
    --env=LANGUAGE="rubygems" \
    --env=DEBUG="true" \
    --env=CODE_INSIGHTS_RESULTS="true"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -ne 0 ]
  echo $output | grep -i "packageManager" | grep -i "rubygems"
  echo $output | grep -i "report succeed generated"
  echo $output | grep -i -v "report error:"
}

@test "Tests and fails on dotnetcore app" {
  run_pipe_container test/fixtures/nuget \
    --env=LANGUAGE="nuget" \
    --env=TARGET_FILE="DotNetCoreTest.sln"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -ne 0 ]
  echo $output | grep -i "package manager" | grep -i "nuget"
  echo $output | grep -i -v "report error:"
  echo $output | grep -i -v "report succeed generated"
}

@test "Tests and fails on dotnetcore app and generate Code Insight report" {
  run_pipe_container test/fixtures/nuget \
    --env=LANGUAGE="nuget" \
    --env=TARGET_FILE="DotNetCoreTest.sln" \
    --env=DEBUG="true" \
    --env=CODE_INSIGHTS_RESULTS="true"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -ne 0 ]
  echo $output | grep -i "packageManager" | grep -i "nuget"
  echo $output | grep -i "report succeed generated"
  echo $output | grep -i -v "report error:"
}

@test "Tests and fails on docker app" {
  local IMAGE_NAME="docker-goof"
  docker build -t $IMAGE_NAME test/fixtures/docker

  run_pipe_container test/fixtures/docker \
    --env=LANGUAGE="docker" \
    --env=IMAGE_NAME="$IMAGE_NAME" \
    --env=TARGET_FILE="Dockerfile"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -ne 0 ]
  echo $output | grep -i "package manager" | grep -i "deb"
  echo $output | grep -i -v "report error:"
  echo $output | grep -i -v "report succeed generated"
}

@test "Tests and fails on docker app for JSON ouput" {
  local IMAGE_NAME="docker-goof"
  docker build -t $IMAGE_NAME test/fixtures/docker

  run_pipe_container test/fixtures/docker \
    --env=LANGUAGE="docker" \
    --env=IMAGE_NAME="$IMAGE_NAME" \
    --env=TARGET_FILE="Dockerfile" \
    --env=DEBUG="true" \
    --env=CODE_INSIGHTS_RESULTS="true"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -ne 0 ]
  echo $output | grep -i "packageManager" | grep -i "deb"
  echo $output | grep -i "report succeed generated"
  echo $output | grep -i -v "report error:"
}

run_pipe_container() {
  FIXTURE_DIR="$(pwd)/$1"

  run docker container run \
    --volume=/usr/local/bin/docker:/usr/local/bin/docker:ro \
    --volume=/var/run/docker.sock:/var/run/docker.sock \
    --env=DOCKER_HOST="$DOCKER_HOST" \
    --add-host="host.docker.internal:$BITBUCKET_DOCKER_HOST_INTERNAL" \
    --env=BITBUCKET_DOCKER_HOST_INTERNAL="$BITBUCKET_DOCKER_HOST_INTERNAL" \
    --workdir=$FIXTURE_DIR \
    --volume=$FIXTURE_DIR:$FIXTURE_DIR \
    --env=BITBUCKET_CLONE_DIR="$FIXTURE_DIR" \
    --env=BITBUCKET_REPO_OWNER="snyk" \
    --env=BITBUCKET_REPO_SLUG="$BITBUCKET_REPO_SLUG" \
    --env=BITBUCKET_COMMIT="$BITBUCKET_COMMIT" \
    --env=BITBUCKET_BUILD_NUMBER="$BITBUCKET_BUILD_NUMBER" \
    --env=SNYK_TOKEN="$SNYK_TOKEN" \
    ${@:2} \
    ${PIPE_IMAGE}
}
