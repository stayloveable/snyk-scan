#!/usr/bin/env bats

setup() {
  PIPE_IMAGE=${DOCKER_IMAGE:="test/snyk-pipe"}
  run docker build -t ${PIPE_IMAGE} .
}

teardown() {
    echo "Teardown happens after each test."
}

@test "Generates report for single-manifiest snyk output json file (goof)" {
  run_pipe_container test/fixtures/snyk-test-json-output \
    --env=SNYK_TEST_JSON_INPUT="multi-manifest.json" \
    --env=CODE_INSIGHTS_RESULTS="true"

  echo "Status: $status"
  echo "Output: $output"

  [ "$status" -eq 0 ]
  echo $output | grep -i "Report succeed generated"
  # this assertion works locally but not in CI:
  # echo $output | grep -i "Code Insight report successfully generated!"
}

@test "Generates report for multi-manifiest snyk output json file (java-goof)" {
 run_pipe_container test/fixtures/snyk-test-json-output \
   --env=SNYK_TEST_JSON_INPUT="multi-manifest.json" \
   --env=CODE_INSIGHTS_RESULTS="true"

 echo "Status: $status"
 echo "Output: $output"

 [ "$status" -eq 0 ]
  echo $output | grep -i "Report succeed generated"
  # this assertion works locally but not in CI:
  # echo $output | grep -i "Code Insight report successfully generated!"
}


# Detects it's local or running in BB and add the correct params
run_pipe_container() {
  FIXTURE_DIR="$(pwd)/$1"

  if [[ -n $BITBUCKET_DOCKER_HOST_INTERNAL ]]; then
    run_pipe_container_ci $1 ${@:2}
  else
    run_pipe_container_local $1 ${@:2}
  fi
}

# Works locally but not in CI
run_pipe_container_local() {
  FIXTURE_DIR="$(pwd)/$1"
  BITBUCKET_REPO_SLUG=my-fake-repo-slug

  run docker container run \
    --volume=/usr/local/bin/docker:/usr/local/bin/docker:ro \
    --volume=/var/run/docker.sock:/var/run/docker.sock \
    --workdir=$FIXTURE_DIR \
    --env=BITBUCKET_REPO_OWNER="snyk" \
    --env=BITBUCKET_REPO_SLUG="$BITBUCKET_REPO_SLUG" \
    --volume=$FIXTURE_DIR:$FIXTURE_DIR \
    --env=BITBUCKET_CLONE_DIR="$FIXTURE_DIR" \
    --env=SNYK_TOKEN="$SNYK_TOKEN" \
    ${@:2} \
    ${PIPE_IMAGE}
}

# Works in CI but not locally
run_pipe_container_ci() {
  FIXTURE_DIR="$(pwd)/$1"

  run docker container run \
    --volume=/usr/local/bin/docker:/usr/local/bin/docker:ro \
    --volume=/var/run/docker.sock:/var/run/docker.sock \
    --env=DOCKER_HOST="$DOCKER_HOST" \
    --add-host="host.docker.internal:$BITBUCKET_DOCKER_HOST_INTERNAL" \
    --env=BITBUCKET_DOCKER_HOST_INTERNAL="$BITBUCKET_DOCKER_HOST_INTERNAL" \
    --workdir=$FIXTURE_DIR \
    --env=BITBUCKET_REPO_OWNER="snyk" \
    --env=BITBUCKET_REPO_SLUG="$BITBUCKET_REPO_SLUG" \
    --volume=$FIXTURE_DIR:$FIXTURE_DIR \
    --env=BITBUCKET_CLONE_DIR="$FIXTURE_DIR" \
    --env=SNYK_TOKEN="$SNYK_TOKEN" \
    ${@:2} \
    ${PIPE_IMAGE}
}
