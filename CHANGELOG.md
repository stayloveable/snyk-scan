# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.3.6

- patch: Code Insight report improviments

## 0.3.5

- patch: Code improvement - unify bitbucket client to use axios

## 0.3.4

- patch: Small update in README to include new option

## 0.3.3

- patch: Include vulnerabilities details in Code Insight report and code improviments

## 0.3.2

- patch: BugFix on code insight report

## 0.3.1

- patch: Add debug logs to help track unexpected errors

## 0.3.0

- minor: Add Code Insight report option to pipe

## 0.2.0

- minor: Add Snyk Protect option to pipe

## 0.1.3

- patch: Small Changes to README.md

## 0.1.2

- patch: Add integration tests

## 0.1.1

- patch: add DEBUG support to cli execution
- patch: rename repo to snyk-scan and add logo

## 0.1.0

- minor: Initial release

